import { Before, HookScenarioResult, BeforeAll } from 'cucumber';
import { info } from '../utils/log';
import { iRegisterUser } from '../steps/authentication/register';
import { USERNAME1, DISPLAYNAME1, LOAD_TIMEOUT, USERNAME2, DISPLAYNAME2, PREFIX } from '../utils/environment';
import { switchToUser } from '../steps/general.steps';
import { switchToLanguageEnglish } from '../steps/core/home.steps';

BeforeAll({ timeout: LOAD_TIMEOUT }, async function () {
	info(`Preparing environment with prefix '${PREFIX}'`);

	await switchToUser(USERNAME1);
	await switchToLanguageEnglish();
	await iRegisterUser(USERNAME1, DISPLAYNAME1);

	await switchToUser(USERNAME2);
	await switchToLanguageEnglish();
	await iRegisterUser(USERNAME2, DISPLAYNAME2);
});

Before(async function (scenario: HookScenarioResult) {
	info(`Running scenario: ${scenario.pickle.name}`);
});
