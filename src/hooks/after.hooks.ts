import { AfterAll } from 'cucumber';
import { stopAllDrivers } from '../interfaces/driver.interface';

AfterAll(async function () {
	await stopAllDrivers();
});
