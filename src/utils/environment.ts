export const MODE = getString('MODE', 'LOCAL');

export const PREFIX = new Date().getTime() + '_';

export const BASE_URL = getString('BASE_URL', 'https://connect-test.kominal.com');
export const LOAD_TIMEOUT = getNumber('LOAD_TIMEOUT', 60000);

export const USERNAME1 = PREFIX + getString('USERNAME1', 'username1');
export const DISPLAYNAME1 = PREFIX + getString('DISPLAYNAME1', 'displayname1');

export const USERNAME2 = PREFIX + getString('USERNAME2', 'username2');
export const DISPLAYNAME2 = PREFIX + getString('DISPLAYNAME2', 'displayname2');

export const PASSWORD = PREFIX + getString('PASSWORD', 'connect-test');

function getString(variable: string, defaultValue?: string): string {
	const value = process.env[variable];
	if (value) {
		return value;
	}
	if (!defaultValue) {
		throw new Error(`Variable '${variable}' is undefined.`);
	}
	return defaultValue;
}

function getNumber(variable: string, defaultValue?: number): number {
	const value = getString(variable, defaultValue?.toString());
	return Number(value);
}
