import { Given } from 'cucumber';
import { BASE_URL, LOAD_TIMEOUT } from '../../utils/environment';
import { open, type, click, waitForElementToHaveText } from '../../interfaces/driver.interface';
import { INPUT_MESSAGE, BUTTON_SEND } from '../../locators/connect/group.locators';
import { By } from 'selenium-webdriver';

export async function openGroupPage(id: string) {
	await open(`${BASE_URL}/connect/group/${id}`);
}

export async function sendMessage(message: string) {
	await type(INPUT_MESSAGE, message);
	await click(BUTTON_SEND);
}

export async function expectMessageText(message: number, messageGroup: number, text: string) {
	await waitForElementToHaveText(
		By.xpath(
			`/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav-content/app-group/div/app-message-group[${messageGroup}]/app-message[${message}]/div[1]/mat-icon`
		),
		'check'
	);
	await waitForElementToHaveText(
		By.xpath(
			`/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav-content/app-group/div/app-message-group[${messageGroup}]/app-message[${message}]/div[2]/span`
		),
		text
	);
}

Given('I open the connect group page for group {string}', { timeout: LOAD_TIMEOUT }, openGroupPage);
Given('I send message {string}', { timeout: LOAD_TIMEOUT }, sendMessage);
Given('I expect the text of message {int} of message group {int} to be {string}', { timeout: LOAD_TIMEOUT }, expectMessageText);
