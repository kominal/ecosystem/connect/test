import { Given } from 'cucumber';
import { LOAD_TIMEOUT } from '../../utils/environment';
import { click } from '../../interfaces/driver.interface';
import { BUTTON_LEAVE, BUTTON_ADD_USER } from '../../locators/connect/details.locators';

export async function openLeaveDialog() {
	await click(BUTTON_LEAVE);
}

export async function openAddUserDialog() {
	await click(BUTTON_ADD_USER);
}

Given('I click the leave group button', { timeout: LOAD_TIMEOUT }, openLeaveDialog);
Given('I click the add user button', { timeout: LOAD_TIMEOUT }, openAddUserDialog);
