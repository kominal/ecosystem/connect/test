import { Given } from 'cucumber';
import { LOAD_TIMEOUT, DISPLAYNAME1, DISPLAYNAME2 } from '../../utils/environment';
import { click, waitForElementToHaveText } from '../../interfaces/driver.interface';
import { BUTTON_DETAILS, BUTTON_CALL_START, GROUP_NAME } from '../../locators/connect/group-bar.locators';

export async function openDetails() {
	await click(BUTTON_DETAILS);
}

export async function startCall() {
	await click(BUTTON_CALL_START);
}

export async function waitForGroupToBeSelected(name: string) {
	name = name.replace('DISPLAYNAME1', DISPLAYNAME1);
	name = name.replace('DISPLAYNAME2', DISPLAYNAME2);
	await waitForElementToHaveText(GROUP_NAME, name);
}

Given('I open the group details', { timeout: LOAD_TIMEOUT }, openDetails);
Given('I wait for group {string} to be selected', { timeout: LOAD_TIMEOUT }, waitForGroupToBeSelected);
Given('I start a call', { timeout: LOAD_TIMEOUT }, startCall);
