import { Given } from 'cucumber';
import { LOAD_TIMEOUT } from '../../utils/environment';
import { click, waitForElementToHaveText, waitForElement } from '../../interfaces/driver.interface';
import {
	BUTTON_VIDEO,
	BUTTON_ENDCALL,
	BUTTON_SCREEN,
	BUTTON_SPEAKER,
	BUTTON_MICROPHONE,
	CHIP_PARTNER,
} from '../../locators/connect/call-bar.locators';
import { until, By } from 'selenium-webdriver';

export async function toggleVideo() {
	await click(BUTTON_VIDEO);
}

export async function toggleScreen() {
	await click(BUTTON_SCREEN);
}

export async function toggleSpeaker() {
	await click(BUTTON_SPEAKER);
}

export async function toggleMicrophone() {
	await click(BUTTON_MICROPHONE);
}

export async function endCall() {
	await new Promise((resolve) => setTimeout(resolve, 30000));
	await click(BUTTON_ENDCALL);
}

export async function waitForPartnerToSpeak() {
	const element = await waitForElement(CHIP_PARTNER);
	for (let i = 0; i < 100; i++) {
		const value = await element.getCssValue('background-color');
		if (value === 'rgba(142, 36, 170, 1)') {
			return;
		}
		await new Promise((resolve) => setTimeout(resolve, 75));
	}
	throw new Error('Partner was not speaking');
}

export async function expectUserChipIcon(index: number, icon: string) {
	await waitForElementToHaveText(By.xpath(`//app-call-bar/mat-toolbar/mat-chip-list/div/mat-chip[1]/mat-icon[${index}]`), icon);
}

export async function expectPartnerChipIcon(index: number, icon: string) {
	await waitForElementToHaveText(By.xpath(`//app-call-bar/mat-toolbar/mat-chip-list/div/mat-chip[2]/mat-icon[${index}]`), icon);
}

Given('I toggle the video', { timeout: LOAD_TIMEOUT }, toggleVideo);
Given('I toggle the screen', { timeout: LOAD_TIMEOUT }, toggleScreen);
Given('I toggle the speaker', { timeout: LOAD_TIMEOUT }, toggleSpeaker);
Given('I toggle the microphone', { timeout: LOAD_TIMEOUT }, toggleMicrophone);
Given('I end the call', { timeout: LOAD_TIMEOUT }, endCall);
Given('I wait for partner to speak', { timeout: LOAD_TIMEOUT }, waitForPartnerToSpeak);
Given('I expect user chip icon {int} to be {string}', { timeout: LOAD_TIMEOUT }, expectUserChipIcon);
Given('I expect partner chip icon {int} to be {string}', { timeout: LOAD_TIMEOUT }, expectPartnerChipIcon);
