import { Given } from 'cucumber';
import { LOAD_TIMEOUT, DISPLAYNAME2, DISPLAYNAME1 } from '../../utils/environment';
import { click, type } from '../../interfaces/driver.interface';
import { BUTTON_CANCEL, BUTTON_ADD_USER, INPUT_DISPLAYNAME } from '../../locators/connect/add-user.dialog.locators';

export async function clickCancel() {
	await click(BUTTON_CANCEL);
}

export async function typeDisplayname(displayname: string) {
	displayname = displayname.replace('DISPLAYNAME1', DISPLAYNAME1);
	displayname = displayname.replace('DISPLAYNAME2', DISPLAYNAME2);
	type(INPUT_DISPLAYNAME, displayname);
}

export async function clickAddUser() {
	await click(BUTTON_ADD_USER);
}

Given('I enter {string} into the add user dialog', { timeout: LOAD_TIMEOUT }, typeDisplayname);
Given('I click the cancel button in the add user dialog', { timeout: LOAD_TIMEOUT }, clickCancel);
Given('I click the add user button in the add user dialog', { timeout: LOAD_TIMEOUT }, clickAddUser);
