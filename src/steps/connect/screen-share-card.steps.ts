import { Given } from 'cucumber';
import { LOAD_TIMEOUT } from '../../utils/environment';
import { waitForElement } from '../../interfaces/driver.interface';
import { SCREEN_SHARE_CARD } from '../../locators/connect/screen-share-card.locators';

export async function expectScreenShareCardVisable() {
	await waitForElement(SCREEN_SHARE_CARD);
}

Given('I expect the screen share card to be visable', { timeout: LOAD_TIMEOUT }, expectScreenShareCardVisable);
