import { Given } from 'cucumber';
import { LOAD_TIMEOUT, DISPLAYNAME2, DISPLAYNAME1 } from '../../utils/environment';
import { click, type } from '../../interfaces/driver.interface';
import { BUTTON_GROUP_FIRST, BUTTON_CREATE, BUTTON_GROUP, BUTTON_DIRECT_MESSAGE } from '../../locators/connect/sidenav.locators';
import { BUTTON_CONFIRM, INPUT_TEXT } from '../../locators/connect/text-with-profile.dialog.locators';
import { By } from 'selenium-webdriver';

export async function openTheFirstGroup() {
	await click(BUTTON_GROUP_FIRST);
}

export async function switchGroup(name: string) {
	name = name.replace('DISPLAYNAME1', DISPLAYNAME1);
	name = name.replace('DISPLAYNAME2', DISPLAYNAME2);
	await click(By.xpath(`//mat-list-item[contains(node(),"${name}")]`));
}

export async function createGroup(name: string) {
	await click(BUTTON_CREATE);
	await click(BUTTON_GROUP);
	await type(INPUT_TEXT, name);
	await click(BUTTON_CONFIRM);
}

export async function startDirectMessage(displayname: string) {
	displayname = displayname.replace('DISPLAYNAME1', DISPLAYNAME1);
	displayname = displayname.replace('DISPLAYNAME2', DISPLAYNAME2);
	await click(BUTTON_CREATE);
	await click(BUTTON_DIRECT_MESSAGE);
	await type(INPUT_TEXT, displayname);
	await click(BUTTON_CONFIRM);
}

Given('I create a group with name {string}', { timeout: LOAD_TIMEOUT }, createGroup);
Given('I switch to group with name {string}', { timeout: LOAD_TIMEOUT }, switchGroup);
Given('I start direct message with user {string}', { timeout: LOAD_TIMEOUT }, startDirectMessage);
