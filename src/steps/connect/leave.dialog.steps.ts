import { Given } from 'cucumber';
import { LOAD_TIMEOUT } from '../../utils/environment';
import { click } from '../../interfaces/driver.interface';
import { BUTTON_CANCEL, BUTTON_LEAVE, BUTTON_DELETE } from '../../locators/connect/leave.dialog.locators';

export async function clickCancel() {
	await click(BUTTON_CANCEL);
}

export async function clickLeave() {
	await click(BUTTON_LEAVE);
}

export async function clickDelete() {
	await click(BUTTON_DELETE);
}

Given('I click the cancel button in the leave dialog', { timeout: LOAD_TIMEOUT }, clickCancel);
Given('I click the leave button in the leave dialog', { timeout: LOAD_TIMEOUT }, clickLeave);
Given('I click the delete button in the leave dialog', { timeout: LOAD_TIMEOUT }, clickDelete);
