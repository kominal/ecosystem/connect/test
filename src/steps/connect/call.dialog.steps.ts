import { Given } from 'cucumber';
import { LOAD_TIMEOUT } from '../../utils/environment';
import { BUTTON_CANCEL, BUTTON_ACCEPT } from '../../locators/connect/call.dialog.locators';
import { click } from '../../interfaces/driver.interface';

export async function clickCancel() {
	await click(BUTTON_CANCEL);
}

export async function clickAccept() {
	await click(BUTTON_ACCEPT);
}

Given('I click the cancel button in the call dialog', { timeout: LOAD_TIMEOUT }, clickCancel);
Given('I click the accept button in the call dialog', { timeout: LOAD_TIMEOUT }, clickAccept);
