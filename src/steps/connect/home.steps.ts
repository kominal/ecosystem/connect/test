import { Given } from 'cucumber';
import { BASE_URL, LOAD_TIMEOUT } from '../../utils/environment';
import { open } from '../../interfaces/driver.interface';

export async function openTheHomePage() {
	await open(`${BASE_URL}/connect/home`);
}

Given('I open the connect home page', { timeout: LOAD_TIMEOUT }, openTheHomePage);
