import { LOAD_TIMEOUT, USERNAME1, USERNAME2 } from '../utils/environment';
import { Given } from 'cucumber';
import { switchDriver } from '../interfaces/driver.interface';

export async function wait(seconds: number) {
	await new Promise((resolve) => {
		setTimeout(resolve, seconds * 1000);
	});
}

export async function switchToUser(username: string) {
	await switchDriver(username);
}

Given('This is not yet implemented', { timeout: LOAD_TIMEOUT }, () => {});
Given('I wait {int} seconds', { timeout: LOAD_TIMEOUT }, wait);
Given('I switch to first user', { timeout: LOAD_TIMEOUT }, () => switchToUser(USERNAME1));
Given('I switch to second user', { timeout: LOAD_TIMEOUT }, () => switchToUser(USERNAME2));
