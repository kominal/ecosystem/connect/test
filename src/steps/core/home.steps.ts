import { BASE_URL } from '../../utils/environment';
import { open, click } from '../../interfaces/driver.interface';
import { BUTTON_LANGUAGE, BUTTON_ENGLISH } from '../../locators/core/home.locators';

export async function openTheHomePage() {
	await open(`${BASE_URL}/core/home`);
}

export async function switchToLanguageEnglish() {
	await openTheHomePage();

	await click(BUTTON_LANGUAGE);
	await click(BUTTON_ENGLISH);
}
