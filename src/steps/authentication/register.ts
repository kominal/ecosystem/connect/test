import { Given } from 'cucumber';
import { BASE_URL, LOAD_TIMEOUT, PASSWORD } from '../../utils/environment';
import { BUTTON_REGISTER, INPUT_USERNAME, INPUT_DISPLAYNAME, INPUT_PASSWORD } from '../../locators/authentication/register.locators';
import { BUTTON_STATUS } from '../../locators/connect/sidenav.locators';
import { open, type, click, waitForElement } from '../../interfaces/driver.interface';

export async function openTheRegisterPage() {
	await open(`${BASE_URL}/authentication/register`);
}

export async function iRegisterUser(username: string, displayname: string) {
	await openTheRegisterPage();

	await waitForElement(BUTTON_REGISTER);
	await type(INPUT_USERNAME, username);
	await type(INPUT_DISPLAYNAME, displayname);
	await type(INPUT_PASSWORD, PASSWORD);
	await click(BUTTON_REGISTER);
	await waitForElement(BUTTON_STATUS);
}

Given('I open the register page', { timeout: LOAD_TIMEOUT }, openTheRegisterPage);
