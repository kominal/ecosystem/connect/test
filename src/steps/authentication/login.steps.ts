import { Given } from 'cucumber';
import { BASE_URL, LOAD_TIMEOUT } from '../../utils/environment';
import { BUTTON_LOGIN, INPUT_USERNAME, INPUT_PASSWORD } from '../../locators/authentication/login.locators';
import { BUTTON_STATUS } from '../../locators/connect/sidenav.locators';
import { open, type, click, waitForElement } from '../../interfaces/driver.interface';

export async function openTheLoginPage() {
	await open(`${BASE_URL}/authentication/login`);
}

export async function iAmLoggedIn() {
	await openTheLoginPage();
	const element = await Promise.race([waitForElement(BUTTON_LOGIN), waitForElement(BUTTON_STATUS)]);
	const text = await element.getText();
	if (text === 'Login') {
		await click(BUTTON_LOGIN);
	}
	await waitForElement(BUTTON_STATUS);
}

Given('I open the login page', { timeout: LOAD_TIMEOUT }, openTheLoginPage);
Given('I am logged in', { timeout: LOAD_TIMEOUT }, iAmLoggedIn);
