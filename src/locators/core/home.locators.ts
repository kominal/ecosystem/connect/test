import { By } from 'selenium-webdriver';

export const BUTTON_LANGUAGE = By.xpath('/html/body/app-root/app-core/mat-toolbar/div[2]/button[2]');

export const BUTTON_ENGLISH = By.xpath('//button[contains(text(),"English")]');
