import { By } from 'selenium-webdriver';

export const INPUT_USERNAME = By.xpath(
	'/html/body/app-root/app-authentication/div/mat-card/app-register/form/mat-form-field[1]/div/div[1]/div[3]/input'
);
export const INPUT_DISPLAYNAME = By.xpath(
	'/html/body/app-root/app-authentication/div/mat-card/app-register/form/mat-form-field[2]/div/div[1]/div[3]/input'
);
export const INPUT_PASSWORD = By.xpath(
	'/html/body/app-root/app-authentication/div/mat-card/app-register/form/mat-form-field[3]/div/div[1]/div[3]/input'
);
export const BUTTON_REGISTER = By.xpath('/html/body/app-root/app-authentication/div/mat-card/app-register/form/button');
