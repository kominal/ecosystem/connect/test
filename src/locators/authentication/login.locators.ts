import { By } from 'selenium-webdriver';

export const INPUT_USERNAME = By.xpath(
	'/html/body/app-root/app-authentication/div/mat-card/app-login/form/mat-form-field[1]/div/div[1]/div[3]/input'
);
export const INPUT_PASSWORD = By.xpath(
	'/html/body/app-root/app-authentication/div/mat-card/app-login/form/mat-form-field[2]/div/div[1]/div[3]/input'
);
export const BUTTON_LOGIN = By.xpath('/html/body/app-root/app-authentication/div/mat-card/app-login/form/button');
