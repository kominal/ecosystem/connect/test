import { By } from 'selenium-webdriver';

export const BUTTON_ATTACHMENT = By.xpath('//app-group/app-input/div[1]/button[1]');
export const INPUT_MESSAGE = By.xpath('//app-group/app-input/div[1]/textarea');
export const BUTTON_EMOJI = By.xpath('//app-group/app-input/div[1]/button[2]');
export const BUTTON_SEND = By.xpath('//app-group/app-input/div[1]/button[3]');
