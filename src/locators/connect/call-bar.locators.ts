import { By } from 'selenium-webdriver';

export const CHIP_USER = By.xpath('//app-call-bar/mat-toolbar/mat-chip-list/div/mat-chip[1]');
export const CHIP_PARTNER = By.xpath('//app-call-bar/mat-toolbar/mat-chip-list/div/mat-chip[2]');

export const BUTTON_VIDEO = By.xpath('//app-call-bar/mat-toolbar/div/button[1]');
export const BUTTON_SCREEN = By.xpath('//app-call-bar/mat-toolbar/div/button[2]');
export const BUTTON_SPEAKER = By.xpath('//app-call-bar/mat-toolbar/div/button[3]');
export const BUTTON_MICROPHONE = By.xpath('//app-call-bar/mat-toolbar/div/button[4]');
export const BUTTON_ENDCALL = By.xpath('//app-call-bar/mat-toolbar/div/button[5]');
