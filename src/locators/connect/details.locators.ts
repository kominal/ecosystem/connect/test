import { By } from 'selenium-webdriver';

export const BUTTON_EDIT = By.xpath('//app-details/mat-toolbar/div/button[contains(node(),"edit")]');
export const BUTTON_ADD_USER = By.xpath('//app-details/mat-toolbar/div/button[contains(node(),"person_add")]');
export const BUTTON_LEAVE = By.xpath('//app-details/mat-toolbar/div/button[contains(node(),"exit_to_app")]');
