import { By } from 'selenium-webdriver';

export const BUTTON_CANCEL = By.xpath('//app-call-dialog/div[2]/button[1]');
export const BUTTON_ACCEPT = By.xpath('//app-call-dialog/div[2]/button[2]');
