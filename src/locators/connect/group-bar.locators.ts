import { By } from 'selenium-webdriver';

export const BUTTON_DETAILS = By.xpath('//app-group-bar/mat-toolbar/div/button[contains(node(),"info")]');
export const BUTTON_CALL_START = By.xpath('//app-group-bar/mat-toolbar/div/button[contains(node(),"call")]');

export const GROUP_NAME = By.xpath('//app-group-bar/mat-toolbar/span');
