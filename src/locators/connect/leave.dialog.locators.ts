import { By } from 'selenium-webdriver';

export const BUTTON_CANCEL = By.xpath('//app-leave-group-dialog/div/button[contains(node(),"Cancel")]');
export const BUTTON_LEAVE = By.xpath('//app-leave-group-dialog/div/button[contains(node(),"Leave")]');
export const BUTTON_DELETE = By.xpath('//app-leave-group-dialog/div/button[contains(node(),"Delete")]');
