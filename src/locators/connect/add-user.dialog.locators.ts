import { By } from 'selenium-webdriver';

export const INPUT_DISPLAYNAME = By.xpath('//app-text-dialog/form/div[1]/mat-form-field/div/div[1]/div[3]/input');
export const BUTTON_CANCEL = By.xpath('//app-text-dialog/form/div[2]/button[2]');
export const BUTTON_ADD_USER = By.xpath('//app-text-dialog/form/div[2]/button[2]');
