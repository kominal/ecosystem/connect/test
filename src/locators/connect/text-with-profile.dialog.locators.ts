import { By } from 'selenium-webdriver';

export const INPUT_TEXT = By.xpath('//app-text-with-profile-dialog/form/div[1]/mat-form-field[1]/div/div[1]/div[3]/input');
export const BUTTON_CANCEL = By.xpath('//app-text-with-profile-dialog/form/div[2]/button[1]');
export const BUTTON_CONFIRM = By.xpath('//app-text-with-profile-dialog/form/div[2]/button[2]');
