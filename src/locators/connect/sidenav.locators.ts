import { By } from 'selenium-webdriver';

export const BUTTON_STATUS = By.xpath(
	'/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav/div/app-sidenav-header/mat-toolbar/button[1]'
);
export const BUTTON_SETTINGS = By.xpath(
	'/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav/div/app-sidenav-header/mat-toolbar/button[2]'
);
export const BUTTON_CREATE = By.xpath(
	'/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav/div/app-sidenav-header/mat-toolbar/button[3]'
);
export const BUTTON_LOGOUT = By.xpath(
	'/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav/div/app-sidenav-header/mat-toolbar/button[4]'
);
export const BUTTON_GROUP_FIRST = By.xpath(
	'/html/body/app-root/app-connect/mat-sidenav-container/mat-sidenav/div/mat-nav-list/mat-list-item[1]'
);

export const BUTTON_DIRECT_MESSAGE = By.xpath('//button[contains(text(),"Direct message")]');
export const BUTTON_GROUP = By.xpath('//button[contains(text(),"Group")]');
