import { Builder, WebDriver, Locator, WebElement, until } from 'selenium-webdriver';
import { Options as FirefoxOptions } from 'selenium-webdriver/firefox';
import { Options as ChromeOptions } from 'selenium-webdriver/chrome';
import { MODE } from '../utils/environment';

let drivers = new Map<string, WebDriver>();
let driver: WebDriver | undefined;

export async function switchDriver(username: string) {
	if (!drivers.has(username)) {
		drivers.set(username, await startADriver());
	}
	driver = drivers.get(username);
}

async function startADriver(): Promise<WebDriver> {
	if (MODE === 'FIREFOX') {
		const firefoxOptions = new FirefoxOptions();
		firefoxOptions.addArguments('--headless');
		driver = await new Builder().setFirefoxOptions(firefoxOptions).forBrowser('firefox').build();
	} else if (MODE === 'CHROME') {
		const chromeOptions = new ChromeOptions();
		chromeOptions.addArguments(
			'--headless',
			'--use-fake-ui-for-media-stream',
			'--use-fake-device-for-media-stream',
			'--disable-translate',
			'--mute-audio'
		);
		driver = await new Builder().setChromeOptions(chromeOptions).forBrowser('chrome').build();
	} else {
		const chromeOptions = new ChromeOptions();
		chromeOptions.addArguments(
			'--width=800',
			'--height=800',
			'--use-fake-ui-for-media-stream',
			'--use-fake-device-for-media-stream',
			'--disable-translate',
			'--mute-audio'
		);
		driver = await new Builder().setChromeOptions(chromeOptions).forBrowser('chrome').build();
	}
	if (!driver) {
		throw new Error('Driver is undefined.');
	}
	if (MODE === 'LOCAL') {
		await driver
			.manage()
			.window()
			.setPosition(10 + drivers.size * 860, 10);
	}
	return driver;
}

export async function open(url: string) {
	if (!driver) {
		throw new Error('Driver is undefined.');
	}
	await driver.get(url);
}

export async function click(locator: Locator) {
	const element = await waitForElement(locator);
	if (!element) {
		throw new Error(`Element '${locator}' not found`);
	}
	await element.click();
}

export async function waitForElement(locator: Locator): Promise<WebElement> {
	const element = await waitForElementWithTimeout(locator, 10);
	if (!element) {
		throw new Error(`Element '${locator}' not found`);
	}
	return element;
}

export async function waitForElementWithTimeout(locator: Locator, seconds: number): Promise<WebElement> {
	if (!driver) {
		throw new Error('Driver is undefined.');
	}
	return await driver.wait(until.elementLocated(locator), seconds * 1000);
}

export async function waitForElementToHaveText(locator: Locator, text: string): Promise<void> {
	await waitForElementToHaveTextWithTimeout(locator, text, 10);
}

export async function waitForElementToHaveTextWithTimeout(locator: Locator, text: string, seconds: number): Promise<void> {
	if (!driver) {
		throw new Error('Driver is undefined.');
	}
	for (let i = 0; i < seconds; i++) {
		try {
			const element = await waitForElementWithTimeout(locator, 1);
			await driver.wait(until.elementTextContains(element, text), 500);
			return;
		} catch (e) {}
	}
	throw new Error('Element text not found.');
}

export async function type(locator: Locator, value: string) {
	if (!driver) {
		throw new Error('Driver is undefined.');
	}
	const element = await waitForElement(locator);
	if (!element) {
		throw new Error(`Element '${locator}' not found`);
	}
	await element.sendKeys(value);
}

export async function stopAllDrivers() {
	for (const driver of Array.from(drivers.values())) {
		await driver.close();
	}
	drivers.clear();
	driver = undefined;
}
