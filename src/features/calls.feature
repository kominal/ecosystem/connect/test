Feature: Calls

	Scenario: I test a call
		And I switch to first user
		And I create a group with name "Call Test 1"
		And I wait for group "Call Test 1" to be selected

		And I open the group details
		And I click the add user button
		And I enter "DISPLAYNAME2" into the add user dialog
		And I click the add user button in the add user dialog
		And I switch to group with name "Call Test 1"
		And I wait for group "Call Test 1" to be selected

		And I switch to second user
		And I switch to group with name "Call Test 1"
		And I wait for group "Call Test 1" to be selected

		And I start a call

		And I switch to first user
		And I click the accept button in the call dialog

		And I wait for partner to speak
		And I switch to second user
		And I wait for partner to speak

		And I toggle the speaker
		And I toggle the microphone
		And I switch to first user
		And I toggle the speaker
		And I toggle the microphone

		And I expect user chip icon 1 to be "mic_off"
		And I expect user chip icon 2 to be "volume_off"
		And I expect partner chip icon 1 to be "mic_off"
		And I expect partner chip icon 2 to be "volume_off"
		And I switch to second user
		And I expect user chip icon 1 to be "mic_off"
		And I expect user chip icon 2 to be "volume_off"
		And I expect partner chip icon 1 to be "mic_off"
		And I expect partner chip icon 2 to be "volume_off"

		And I toggle the screen

		And I expect the screen share card to be visable
		And I expect user chip icon 1 to be "personal_video"
		And I switch to first user
		And I expect the screen share card to be visable
		And I expect partner chip icon 1 to be "personal_video"

		And I end the call
		And I switch to second user
		And I end the call
