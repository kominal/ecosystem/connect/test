Feature: Groups

	Scenario: I create a group
		And I switch to first user
		And I create a group with name "Test group 1"
		And I wait for group "Test group 1" to be selected
		And I send message "First test message"

		And I open the group details
		And I click the add user button
		And I enter "DISPLAYNAME2" into the add user dialog
		And I click the add user button in the add user dialog
		And I switch to group with name "Test group 1"
		And I wait for group "Test group 1" to be selected

		And I switch to second user
		And I switch to group with name "Test group 1"
		And I wait for group "Test group 1" to be selected
		And I send message "Second test message"

		Then I expect the text of message 1 of message group 1 to be "Second test message"

		And I switch to first user
		Then I expect the text of message 1 of message group 1 to be "First test message"
		Then I expect the text of message 1 of message group 2 to be "Second test message"
